#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QComboBox>

#include "firstpage.hpp"
#include "secondpage.hpp"
#include "thirdpage.hpp"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  rootPageWidget = new QWidget;

  layoutRootPageWidget = new QVBoxLayout(rootPageWidget);

  pageComboBox = new QComboBox(this);
  pageComboBox->addItem(tr("Page 1"));
  pageComboBox->addItem(tr("Page 2"));
  pageComboBox->addItem(tr("Page 3"));

  connect(pageComboBox, SIGNAL(activated(int)),
          this, SLOT(setCurrentIndex(int)));

  pageWidget = new FirstPage(this);

  layoutRootPageWidget->addWidget(pageComboBox);
  layoutRootPageWidget->addWidget(pageWidget);

  setCentralWidget(rootPageWidget);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::setCurrentIndex(int value)
{
    qDebug() << value;

    layoutRootPageWidget->removeWidget(pageWidget);

    delete pageWidget;
    pageWidget = nullptr;

    switch (value) {
      case 0:
        pageWidget = new FirstPage(this);
        break;
      case 1:
        pageWidget = new SecondPage(this);
        break;
      case 2:
        pageWidget = new ThirdPage(this);
        break;
      default:
        break;
    }

    layoutRootPageWidget->addWidget(pageWidget);
}

void MainWindow::moveToSecondPage()
{
  qDebug() << "moveToSecondPage";

  setCurrentIndex(1);
  pageComboBox->setCurrentIndex(1);
}

void MainWindow::moveToThirdPage()
{
  qDebug() << "moveToThirdPage";

  setCurrentIndex(2);
  pageComboBox->setCurrentIndex(2);
}

void MainWindow::moveToFirstPage()
{
  qDebug() << "moveToFirstPage";

  setCurrentIndex(0);
  pageComboBox->setCurrentIndex(0);
}

