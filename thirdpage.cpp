#include "thirdpage.hpp"
#include "ui_thirdpage.h"

#include <QDebug>

ThirdPage::ThirdPage(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ThirdPage)
{
  ui->setupUi(this);

  connect(this, SIGNAL(moveToFirstPage()), parent, SLOT(moveToFirstPage()));
}

ThirdPage::~ThirdPage()
{
  delete ui;
}

void ThirdPage::on_ThirdPageButton_clicked()
{
  qDebug() << "on_ThirdPageButton_clicked";

  emit moveToFirstPage();
}
