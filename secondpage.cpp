#include "secondpage.hpp"
#include "ui_secondpage.h"

#include <QDebug>

SecondPage::SecondPage(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::SecondPage)
{
  ui->setupUi(this);

  connect(this, SIGNAL(moveToThirdPage()), parent, SLOT(moveToThirdPage()));
}

SecondPage::~SecondPage()
{
  delete ui;
}

void SecondPage::on_SecondPageButton_clicked()
{
    qDebug() << "on_SecondPageButton_clicked";

    emit moveToThirdPage();
}
