#ifndef FIRSTPAGE_HPP
#define FIRSTPAGE_HPP

#include <QWidget>
#include <QVBoxLayout>

namespace Ui {
class FirstPage;
}

class FirstPage : public QWidget
{
  Q_OBJECT

public:
  explicit FirstPage(QWidget *parent = 0);
  ~FirstPage();

private slots:
  void on_FirstPageButton_clicked();

signals:
  void moveToSecondPage();

private:
  Ui::FirstPage *ui;
};

#endif // FIRSTPAGE_HPP
