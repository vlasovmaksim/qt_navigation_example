#-------------------------------------------------
#
# Project created by QtCreator 2016-11-19T13:19:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WindowsSwitchMultiple
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    firstpage.cpp \
    secondpage.cpp \
    thirdpage.cpp

HEADERS  += mainwindow.hpp \
    firstpage.hpp \
    secondpage.hpp \
    thirdpage.hpp

FORMS    += mainwindow.ui \
    firstpage.ui \
    secondpage.ui \
    thirdpage.ui
