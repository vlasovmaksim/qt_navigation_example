#ifndef THIRDPAGE_HPP
#define THIRDPAGE_HPP

#include <QWidget>

namespace Ui {
class ThirdPage;
}

class ThirdPage : public QWidget
{
  Q_OBJECT

public:
  explicit ThirdPage(QWidget *parent = 0);
  ~ThirdPage();

private slots:
  void on_ThirdPageButton_clicked();

signals:
  void moveToFirstPage();

private:
  Ui::ThirdPage *ui;
};

#endif // THIRDPAGE_HPP
