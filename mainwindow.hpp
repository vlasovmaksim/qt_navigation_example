#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QVBoxLayout>
#include <QPushButton>
#include <QComboBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:
  void setCurrentIndex(int value);
  void moveToSecondPage();
  void moveToThirdPage();
  void moveToFirstPage();

public:

  Ui::MainWindow *ui;

  QWidget * rootPageWidget = nullptr;

  QWidget * pageWidget = nullptr;

  QPushButton * secondPageButton = nullptr;

  QVBoxLayout * layoutRootPageWidget = nullptr;

  QComboBox * pageComboBox = nullptr;
};

#endif // MAINWINDOW_HPP
