#include "firstpage.hpp"
#include "ui_firstpage.h"

#include <QDebug>

FirstPage::FirstPage(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::FirstPage)
{
  ui->setupUi(this);

  connect(this, SIGNAL(moveToSecondPage()), parent, SLOT(moveToSecondPage()));
}

FirstPage::~FirstPage()
{
  delete ui;
}

void FirstPage::on_FirstPageButton_clicked()
{
    qDebug() << "on_FirstPageButton_clicked";

    emit moveToSecondPage();
}
