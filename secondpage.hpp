#ifndef SECONDPAGE_HPP
#define SECONDPAGE_HPP

#include <QWidget>

namespace Ui {
class SecondPage;
}

class SecondPage : public QWidget
{
  Q_OBJECT

public:
  explicit SecondPage(QWidget *parent = 0);
  ~SecondPage();

private slots:
  void on_SecondPageButton_clicked();

signals:
  void moveToThirdPage();

private:
  Ui::SecondPage *ui;
};

#endif // SECONDPAGE_HPP
